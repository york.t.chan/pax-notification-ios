//
//  Utils.swift
//  NotificationPOC
//
//  Created by Accenture on 4/1/2018.
//  Copyright © 2018 Accenture. All rights reserved.
//

import Foundation

func stringToDatetime(_ dateStr : String) -> Date?
{
    let fmt = DateFormatter()
    fmt.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
    return fmt.date(from: dateStr)
}

func datetimeToString(_ datetime : Date) -> String?
{
    let fmt = DateFormatter()
    fmt.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
    return fmt.string(from: datetime)
}

func stringToDate(_ dateStr : String) -> Date?
{
    let fmt = DateFormatter()
    fmt.dateFormat = "yyyy-MM-dd"
    return fmt.date(from: dateStr)
}

func dateToString(_ date : Date) -> String?
{
    let fmt = DateFormatter()
    fmt.dateFormat = "yyyy-MM-dd"
    return fmt.string(from: date)
}

func julianToDate(_ julian : String) -> Date?
{
    let fmt = DateFormatter()
    fmt.dateFormat = "DDD"
    return fmt.date(from: julian)
}

func deviceTokenToString(_ deviceToken : Data) -> String {
    let tokenParts = deviceToken.map { data -> String in
        return String(format: "%02.2hhx", data)
    }
    
    let token = tokenParts.joined()
    return token
}
