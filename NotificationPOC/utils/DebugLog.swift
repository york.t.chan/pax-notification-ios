//
//  DebugLog.swift
//  BeaconPOC
//
//  Created by Accenture on 5/7/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import SSZipArchive

class DebugLog {

    static var isFileLogEnabled = true
    static var folderName = "DebugLog"
    private static var debugLogFileName = "DebugLog"
    private static var rawLogFileName = "RawLog"
    
    static var didDebugLogTextChanged : (() -> ())?
    static var logText : String {
        get {
            return getFileLog(debugLogFileName)
        }
        set(text) {
            didDebugLogTextChanged?()
            if isFileLogEnabled {
                fileLog(fileNamed: debugLogFileName, text: text)
            }
        }
    }
    
    static var didRawLogTextChanged : (() -> ())?
    static var rawText : String {
        get {
            return getFileLog(rawLogFileName)
        }
        set(text) {
            didRawLogTextChanged?()
            if isFileLogEnabled {
                fileLog(fileNamed: rawLogFileName, text: text)
            }
        }
    }
    
    static func fileLog(fileNamed: String, text: String) {
        let name = String(format:"%@(%@)",fileNamed, dateToString(datetime: Date(), format:"yyyyMMdd"))
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { return }
        guard let writePath = NSURL(fileURLWithPath: path).appendingPathComponent(folderName) else { return }
        try? FileManager.default.createDirectory(atPath: writePath.path, withIntermediateDirectories: true)
        let file = writePath.appendingPathComponent(name + ".txt")
        try? text.write(to: file, atomically: false, encoding: String.Encoding.utf8)
    }
    
    static func getFileLog(_ fileNamed: String) -> String {
        let name = String(format:"%@(%@)",fileNamed, dateToString(datetime: Date(), format:"yyyyMMdd"))
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { return "" }
        guard let readPath = NSURL(fileURLWithPath: path).appendingPathComponent(folderName) else { return "" }
        try? FileManager.default.createDirectory(atPath: readPath.path, withIntermediateDirectories: true)
        let file = readPath.appendingPathComponent(name + ".txt")
        do {
            let text = try String(contentsOf: file, encoding: String.Encoding.utf8)
            return text
        }
        catch {
            return ""
        }
    }
    
    static func getDebugLogZip() -> String? {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { return nil }
        guard let zipPath = NSURL(fileURLWithPath: path).appendingPathComponent(String(format: "%@.zip", folderName)) else { return nil }
        guard let sampleDataPath = NSURL(fileURLWithPath: path).appendingPathComponent(folderName) else { return nil }
        let success = SSZipArchive.createZipFile(atPath: zipPath.relativePath, withContentsOfDirectory: sampleDataPath.relativePath)
        return success ? zipPath.relativePath : nil
    }
    
}

public func printRaw(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let output = items.map { "*\($0)" }.joined(separator: separator)
    DebugLog.rawText = String(format:"%@\n%@ : %@",DebugLog.rawText,dateToString(datetime: Date(), format:"yyyy/MM/dd HH:mm:ss"),output)
    Swift.print(output, terminator: terminator)
}

public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let output = items.map { "*\($0)" }.joined(separator: separator)
    DebugLog.logText = String(format:"%@\n%@ : %@",DebugLog.logText,dateToString(datetime: Date(), format:"yyyy/MM/dd HH:mm:ss"),output)
    Swift.print(output, terminator: terminator)
}

public func dateToString(datetime : Date, format : String) -> String
{
    let fmt = DateFormatter()
    fmt.dateFormat = format
    return fmt.string(from: datetime)
}


