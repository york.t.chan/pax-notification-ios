//
//  UIAlertView.swift
//  BeaconPOC
//
//  Created by Accenture on 24/7/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import UIKit

class UIAlertView {
    static func show(title: String?, message: String?, actions: [UIAlertAction]) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        for action in actions {
            alertController.addAction(action)
        }
        
        let alertWindow = UIWindow.init(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController.init()
        alertWindow.windowLevel = UIWindowLevelAlert + 1
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
