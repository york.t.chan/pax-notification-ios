//
//  Device.swift
//  BeaconPOC
//
//  Created by Accenture on 28/6/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import KeychainSwift
import DeviceGuru 

extension UIDevice {
    
    private var deviceIdKey : String {
        get {
            return "deviceId"
        }
    }
    
    var deviceId : String {
        get {
            let keychain = KeychainSwift()
            if let deviceIdFromKeyChain = keychain.get(deviceIdKey) {
                return deviceIdFromKeyChain
            }
            else {
                let newDeviceId = (self.identifierForVendor?.uuidString)!
                keychain.set(newDeviceId, forKey: deviceIdKey)
                return newDeviceId
            }
        }
    }
    
    func hardwareModel() -> String{
        return DeviceGuru().hardwareDescription()!
    }
}
