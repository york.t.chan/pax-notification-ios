//
//  BoardingPassListCell.swift
//  NotificationPOC
//
//  Created by Accenture on 31/12/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import UIKit

class BoardingPassListCell : UITableViewCell {
    
    @IBOutlet weak var rlocLabel: UILabel!
    @IBOutlet weak var flightNoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sequenceNumberLabel: UILabel!
    @IBOutlet weak var changedLabel: UILabel!
    
    func updateDisplayedInfo(_ record: BarcodeRecord) {
        rlocLabel.text = record.rloc
        flightNoLabel.text = String(format: "%@ %@", record.airline!, record.flightNumber!)
        nameLabel.text = String(format: "%@ %@", record.firstName!, record.lastName!)
        sequenceNumberLabel.text = String(format: "%d", record.sequenceNumber)
        changedLabel.isHidden = !record.isChanged()
    }
}
