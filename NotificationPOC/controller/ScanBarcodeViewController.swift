//
//  ViewController.swift
//  NotificationPOC
//
//  Created by Accenture on 18/11/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import UIKit
import AVFoundation
import ObjectMapper
import Firebase
import SVProgressHUD

class ScanBarcodeViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var progressView : UIView!
    
    var timer = Timer()
    let captureSession = AVCaptureSession()
    let scanLine = UIImageView(image: UIImage(named: "ScanBP_Scanning_Icon.png"))
    let scanRectWidth = CGFloat(250)
    let scanRectHeight = CGFloat(360)
    var scanLineY: CGFloat = 0
    var decodedBarcode : Barcode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        view.addSubview(progressView)
        setCoverView()
        setUpCaptureDevice()
    }
    
    func setCoverView() {
        let maskLayer = CALayer()
        maskLayer.frame = CGRect(x: 0, y: progressView.frame.maxY, width: view.frame.size.width, height: view.frame.size.height - progressView.frame.maxY)
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.opacity = 0.6
        view.layer.addSublayer(maskLayer)
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = view.layer.frame
        let path = UIBezierPath(rect: CGRect(x: (view.frame.width - scanRectWidth)/2, y: (view.frame.height - scanRectHeight - 64)/2, width: scanRectWidth, height: scanRectHeight))
        scanLineY = (view.frame.height - scanRectHeight - 64)/2 + 25
        path.append(UIBezierPath(rect: view.layer.frame))
        shapeLayer.path = path.cgPath
        shapeLayer.fillRule = kCAFillRuleEvenOdd
        maskLayer.mask = shapeLayer
        scanLine.frame.origin.x = (view.frame.width - scanRectWidth)/2 - 3
        scanLine.frame.origin.y = scanLineY
        view.addSubview(scanLine)
    }
    
    @IBAction func goHome(_ sender: Any) {
        let _ = navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureSession.startRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(moveScannerLayer(_:)), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer.invalidate()
        scanLine.frame.origin.y = scanLineY
        
    }
    
    @objc func moveScannerLayer(_ timer : Timer) {
        UIView.animate(withDuration: 2.0) {
            
            if abs(self.scanLine.frame.origin.y - self.scanLineY) < 5 {
                self.scanLine.frame.origin.y = self.scanLineY + self.scanRectHeight - self.scanLine.frame.size.height
            }else{
                self.scanLine.frame.origin.y = self.scanLineY
            }
        }
    }
    
    func setUpCaptureDevice() {
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        let input :AVCaptureDeviceInput
        let output = AVCaptureMetadataOutput()
        
        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                captureSession.removeInput(input)
            }
        }
        
        if let outputs = captureSession.outputs as? [AVCaptureOutput] {
            for output in outputs {
                captureSession.removeOutput(output)
            }
        }
        
        do{
            input = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession.addInput(input)
            captureSession.addOutput(output)
        }catch {
            print("异常")
        }
        
        let dispatchQueue = DispatchQueue(label: "queue", attributes: [])
        output.setMetadataObjectsDelegate(self, queue: dispatchQueue)
        
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
            
        case .authorized:
            setMetadataObjectTypes(output: output)
            break
            
        case .notDetermined:
            print("***** notDetermined")
            dispatchQueue.suspend()
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [unowned self] granted in
                if !granted {
                    self.configCaptureDeviceErrorTip()
                }
                dispatchQueue.resume()
            })
            setMetadataObjectTypes(output: output)
            break
            
        default:
            configCaptureDeviceErrorTip()
            break
        }
        
        let videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(videoPreviewLayer, at: 0)
        output.rectOfInterest = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
    }
    
    func scannerStop() {
        captureSession.stopRunning()
        timer.invalidate()
    }
    
    func scannerStart() {
        captureSession.startRunning()
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(moveScannerLayer(_:)), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    func setMetadataObjectTypes(output: AVCaptureMetadataOutput?) {
        output?.metadataObjectTypes = [AVMetadataObject.ObjectType.upce,
                                       AVMetadataObject.ObjectType.code39,
                                       AVMetadataObject.ObjectType.code39Mod43,
                                       AVMetadataObject.ObjectType.code93,
                                       AVMetadataObject.ObjectType.code128,
                                       AVMetadataObject.ObjectType.ean8,
                                       AVMetadataObject.ObjectType.ean13,
                                       AVMetadataObject.ObjectType.aztec,
                                       AVMetadataObject.ObjectType.pdf417,
                                       AVMetadataObject.ObjectType.qr]
    }
    
    func configCaptureDeviceErrorTip() {
        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    //MARK - AVCaptureMetadataOutputObjectsDelegate
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count > 0 {
            let metaData : AVMetadataMachineReadableCodeObject = metadataObjects.first as! AVMetadataMachineReadableCodeObject
            self.scannerStop()
            
            let barCode = metaData.stringValue!
            handleScanBoardingPass(barCode: barCode)
            
        }
    }
    
    //MARK - scan boarding pass
    func handleScanBoardingPass(barCode: String?) {
        print("barCode : ", barCode ?? "")
        
        if let code = barCode {
            decodedBarcode = Barcode(barcodeData: code, of: PDF_417)
            print("decoded : ", decodedBarcode?.description() ?? "")

            DispatchQueue.main.async {
                SVProgressHUD.show()
                self.regiserCheckInRecord(success: { (statusCode) in
                    self.registerDevice(success: { (statusCode) in
                        SVProgressHUD.dismiss()
                        UIAlertView.show(title: "Success", message: "Submit successfully", actions: [UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                            
                            let nav = self.presentingViewController as! UINavigationController
                            let controller = nav.topViewController as! RecordListViewController
                            self.dismiss(animated: true, completion: {
                                
                                controller.syncWithServer(success: {
                                    controller.selectRecordByBarcode(self.decodedBarcode!)
                                }, failure: { (err) in
                                    UIAlertView.show(title: "Error", message: "Fail to synchronize with server", actions: [UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)])
                                })
                            })
                        })])
                    }, failure: { (statusCode, err) in
                        SVProgressHUD.dismiss()
                        UIAlertView.show(title: "Error",
                                         message: "Fail to submit device",
                                         actions:[UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                                            
                                            self.dismiss(animated: true, completion: nil)
                                         })])
                    })
                }, failure: { (statusCode, err) in
                    SVProgressHUD.dismiss()
                    UIAlertView.show(title: "Error", message: "Fail to submit check in record", actions: [UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        self.dismiss(animated: true, completion: nil)
                    })])
                })
            }
        }
    }
    
    @IBAction func dismiss() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                
            })
        }
    }
    
    // MARK: API
    func regiserCheckInRecord(success: ((Int?) -> Void)? = nil,
                              failure: ((Int?, Error) -> Void)? = nil) {
        
        NetworkManager.shared.registerCheckInRecord(
            rloc: (decodedBarcode?.rloc)!,
            airline: (decodedBarcode?.opAirline)!,
            flightNumber: (decodedBarcode?.flightNo)!,
            flightDate: (decodedBarcode?.dateOfFlightString())!,
            firstName: (decodedBarcode?.firstName)!,
            lastName: (decodedBarcode?.lastName)!,
            memberNumber: decodedBarcode?.memberNo ?? "",
            memberGrade: decodedBarcode?.memberGrade ?? "",
            sequenceNumber: Int((decodedBarcode?.seqNo)!)!,
            success: success,
            failure: failure)
    }
    
    func registerDevice(success: ((Int?) -> Void)? = nil,
                        failure: ((Int?, Error) -> Void)? = nil) {
        
        let token = deviceTokenToString(Messaging.messaging().apnsToken!)
        
        NetworkManager.shared.registerDevice(
            rloc: (decodedBarcode?.rloc)!,
            airline: (decodedBarcode?.opAirline)!,
            flightNumber: (decodedBarcode?.flightNo)!,
            flightDate: (decodedBarcode?.dateOfFlightString())!,
            firstName: (decodedBarcode?.firstName)!,
            lastName: (decodedBarcode?.lastName)!,
            sequenceNumber: Int((decodedBarcode?.seqNo)!)!,
            deviceId: UIDevice.current.deviceId,
            platform: 1,
            osVersion: UIDevice.current.systemVersion,
            model: UIDevice.current.hardwareModel(),
            deviceToken: token,
            fcmToken: Messaging.messaging().fcmToken!,
            success: success,
            failure: failure)
    }
}


