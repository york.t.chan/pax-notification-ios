//
//  RecordListViewController.swift
//  NotificationPOC
//
//  Created by Accenture on 28/12/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import SVProgressHUD

class RecordListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    var datasource : [BarcodeRecord]?
    private let refreshControl = UIRefreshControl()
    
    // MARK: lifecycle
    override func viewDidLoad() {

        syncWithServer()
        
        // Configure Refresh Control
        let color = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        refreshControl.tintColor = color
        let attributes = [ NSAttributedStringKey.foregroundColor: color ]
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing...", attributes: attributes)
        refreshControl.addTarget(self, action: #selector(syncWithServer), for: .valueChanged)
        
        tableview.refreshControl = refreshControl
        tableview.rowHeight = 40;
        tableview.rowHeight = UITableViewAutomaticDimension;
        
        print("addobserver")
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification), name: Notification.Name("RecordUpdateNotification"), object: nil)
    }
    
    deinit {
        print("removeobserver")
        NotificationCenter.default.removeObserver(self, name: Notification.Name("RecordUpdateNotification"), object: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowDetail" {
            
            let indexPath = tableview.indexPathForSelectedRow
            let row = indexPath?.row
            let barcodeRecord : BarcodeRecord = datasource![row!]
            tableview.deselectRow(at: indexPath!, animated: false)
            
            let controller : DetailViewController = segue.destination as! DetailViewController
            controller.barcodeRecord = barcodeRecord
        }
    }
    
    // MARK: tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : BoardingPassListCell = tableview.dequeueReusableCell(withIdentifier: "BoardingPassListCell", for: indexPath) as! BoardingPassListCell
        let barcodeRecord = datasource![indexPath.row]
        cell.updateDisplayedInfo(barcodeRecord)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            UIAlertView.show(title: "Alert", message: "Are you sure to delete this record?", actions: [UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil), UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
                
                let barcodeRecord = self.datasource![indexPath.row]
                SVProgressHUD.show()
                self.unregisterDevice(barcodeRecord, success: { (statusCode) in
                    SVProgressHUD.dismiss()
                    barcodeRecord.delete()
                    self.syncWithServer()
                }, failure: { (statusCode, err) in
                    SVProgressHUD.dismiss()
                    UIAlertView.show(title: "Error", message: "Fail to delete boarding pass", actions: [UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)])
                })
                
            })])
        }
    }
    
    // MARK: datasource
    @objc func syncWithServer(success: (() -> Void)? = nil,
                        failure: ((Error) -> Void)? = nil) {
        
        SVProgressHUD.show()
        NetworkManager.shared.getCheckInRecordList(success: { (statusCode, json) in
            SVProgressHUD.dismiss()
            
            self.updateData(json: json)
            
            success?()
            
        }) { (statusCode, err) in
            SVProgressHUD.dismiss()
            failure?(err)
        }
    }
    
    func updateData(json : [[String:Any?]]) {
        for obj in json {
            
            BarcodeRecord.updateOrAdd(rloc: obj["rloc"] as! String,
                                      airline: obj["airline"] as! String,
                                      flightNumber: obj["flight_number"] as! String,
                                      flightDate: obj["flight_date"] as! String,
                                      firstName: obj["first_name"] as! String,
                                      lastName: obj["last_name"] as! String,
                                      sequenceNumber: Int16(obj["sequence_number"] as! Int),
                                      deviceId: obj["device_id"] as! String,
                                      platform: Int16(obj["platform"] as! Int),
                                      osVersion: obj["os_version"] as! String,
                                      model: obj["model"] as! String,
                                      memberNumber: obj["member_number"] as! String,
                                      memberGrade: obj["member_grade"] as! String,
                                      scheduledDepartureDatetime: obj["scheduled_departure_datetime"] as? String,
                                      estimatedDepartureDatetime: obj["estimated_departure_datetime"] as? String,
                                      gateNumber: obj["gate_number"] as? String,
                                      departureAirport: obj["departure_airport"] as! String,
                                      arrivalAirport: obj["arrival_airport"] as! String,
                                      boardingStatus: Int16(obj["boarding_status"] as! Int))
        }
        datasource = BarcodeRecord.selectAll()!
        tableview.reloadData()
    }
    
    func selectRecordByBarcode(_ newCode : Barcode) {
        let rloc = newCode.rloc.trimmingCharacters(in: .whitespacesAndNewlines)
        let record = BarcodeRecord.selectRecord(rloc: rloc, firstName: newCode.firstName, lastName: newCode.lastName, airline: newCode.opAirline, flightNumber: newCode.flightNo, flightDate: newCode.dateOfFlightString(), sequenceNumber: Int16(newCode.seqNo)!)
        
        if let r = record {
            let row = datasource?.index(of: r)
            tableview.selectRow(at: IndexPath(row: row!, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
            let cell = self.tableview.cellForRow(at: tableview.indexPathForSelectedRow!)
            self.performSegue(withIdentifier: "ShowDetail", sender: cell)
        }
        else {
            UIAlertView.show(title: "Error", message: "Cannot find the new barcode", actions: [UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)])
        }
    }
    
    // MARK: Notification
    @objc func handleNotification() {
        
        print("handleNotification")
        UIAlertView.show(title: "Alert", message: "The flight information is changed", actions: [UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            
            if let controller = self.presentedViewController {
                controller.dismiss(animated: true, completion: nil)
            }
            self.navigationController?.popToRootViewController(animated: true)
            self.syncWithServer()
        })])
    }
    
    // MARK: API
    func unregisterDevice(_ barcodeRecord : BarcodeRecord,
                          success: ((Int?) -> Void)? = nil,
                          failure: ((Int?, Error) -> Void)? = nil) {
        NetworkManager.shared.unregisterDevice(
            rloc: (barcodeRecord.rloc)!,
            airline: (barcodeRecord.airline)!,
            flightNumber: (barcodeRecord.flightNumber)!,
            flightDate: (barcodeRecord.flightDate)!,
            firstName: (barcodeRecord.firstName)!,
            lastName: (barcodeRecord.lastName)!,
            sequenceNumber: Int(barcodeRecord.sequenceNumber),
            deviceId: UIDevice.current.deviceId,
            platform: 1,
            osVersion: UIDevice.current.systemVersion,
            model: UIDevice.current.hardwareModel(),
            success: success,
            failure: failure)
    }
}
