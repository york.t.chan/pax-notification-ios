//
//  FlightInfoViewController.swift
//  NotificationPOC
//
//  Created by Accenture on 20/11/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    
    @IBOutlet weak var lblRLoc: UILabel!
    @IBOutlet weak var lblAirline: UILabel!
    @IBOutlet weak var lblFlightNumber: UILabel!
    @IBOutlet weak var lblFlightDate: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblSeqNo: UILabel!
    @IBOutlet weak var lblMemberNo: UILabel!
    @IBOutlet weak var lblMemberGrade: UILabel!
    @IBOutlet weak var lblScheduledDepartureDate: UILabel!
    @IBOutlet weak var lblEstimateDepartureDate: UILabel!
    @IBOutlet weak var lblGateNumber: UILabel!
    @IBOutlet weak var lblDepartureAirport: UILabel!
    @IBOutlet weak var lblArrvalAirport: UILabel!
    @IBOutlet weak var lblBordingStatus: UILabel!
    @IBOutlet weak var lblDeviceID: UILabel!
    @IBOutlet weak var lblPlatform: UILabel!
    @IBOutlet weak var lblOSVersion: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    
    var barcodeRecord : BarcodeRecord?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let record = barcodeRecord {
            updateDisplayedInfoWithBarcodeRecord(record)
            
            print(String(format: "isBoardingStatusChanged : %@, isScheduledDepartureTimeChanged : %@, isEstimatedDepartureTimeChanged : %@, isGateChanged : %@", String(record.isBoardingStatusChanged()), String(record.isScheduledDepartureTimeChanged()), String(record.isEstimatedDepartureTimeChanged()), String(record.isGateChanged())))
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let record = barcodeRecord {
            record.resetStatusField()
            if (self.navigationController?.viewControllers.count)! > 0 {
                let controller = self.navigationController?.viewControllers[0] as! RecordListViewController
                controller.syncWithServer()
            }
        }
    }
    
    func updateDisplayedInfoWithBarcodeRecord(_ bc : BarcodeRecord) {
        lblRLoc.text = checkEmptyOrNil(bc.rloc)
        lblAirline.text = checkEmptyOrNil(bc.airline)
        lblFlightNumber.text = checkEmptyOrNil(bc.flightNumber)
        lblFlightDate.text = checkEmptyOrNil(bc.flightDate)
        lblLastName.text = checkEmptyOrNil(bc.lastName)
        lblFirstName.text = checkEmptyOrNil(bc.firstName)
        lblSeqNo.text = String(bc.sequenceNumber)
        lblMemberNo.text = checkEmptyOrNil(bc.memberNumber)
        lblMemberGrade.text = checkEmptyOrNil(bc.memberGrade)
        lblScheduledDepartureDate.text = checkEmptyOrNil(bc.scheduledDepartureDatetime)
        lblEstimateDepartureDate.text = checkEmptyOrNil(bc.estimatedDepartureDatetime)
        lblGateNumber.text = checkEmptyOrNil(bc.gateNumber)
        lblDepartureAirport.text = checkEmptyOrNil(bc.departureAirport)
        lblArrvalAirport.text = checkEmptyOrNil(bc.arrivalAirport)
        lblBordingStatus.text = BoardingStatus(rawValue: bc.boardingStatus)?.description
        lblDeviceID.text = checkEmptyOrNil(bc.deviceId)
        lblPlatform.text = String(bc.platform)
        lblOSVersion.text = checkEmptyOrNil(bc.osVersion)
        lblModel.text = checkEmptyOrNil(bc.model)
        
        if bc.isGateChanged() {
            lblGateNumber.textColor = UIColor.red;
        }
        
        if bc.isBoardingStatusChanged() {
            lblBordingStatus.textColor = UIColor.red;
        }
        
        if bc.isEstimatedDepartureTimeChanged() {
            lblEstimateDepartureDate.textColor = UIColor.red;
        }
        
        if bc.isScheduledDepartureTimeChanged() {
            lblScheduledDepartureDate.textColor = UIColor.red;
        }
    }
    
    func checkEmptyOrNil(_ valStr : String?) -> String {
        if let text = valStr, !text.isEmpty {
            return text
        } else {
            return "*Nil or empty"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
