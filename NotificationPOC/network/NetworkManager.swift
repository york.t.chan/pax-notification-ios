//
//  File.swift
//  BeaconPOC
//
//  Created by Accenture on 22/6/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import Alamofire

typealias NetworkManagerSuccessCallback = ((Double)->Void)?

class NetworkManager {
    
    private static let host = "https://pax-notification-pdt-d0.osc1.ct1.cathaypacific.com"
    private let manager = NetworkReachabilityManager(host: host)
    
    private init() {
        
        manager?.listener = { status in
            
            if status != NetworkReachabilityManager.NetworkReachabilityStatus.unknown &&
                status != NetworkReachabilityManager.NetworkReachabilityStatus.notReachable {
                
                
            }
            
            print("Network Status Changed: \(status)")
        }
        
        manager?.startListening()
    }
    static let shared = NetworkManager()
    
    private func sendRequests(urlString: String, params: [String : Any]?, httpMethod: HTTPMethod = .post, success: ((DataResponse<Any>) -> Void)? = nil, failure: ((DataResponse<Any>, Error) -> Void)? = nil) {
        
        let url = NetworkManager.host+urlString
        Alamofire.request(url,
                          method: httpMethod,
                          parameters: params)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON(completionHandler: { res in
                switch res.result {
                case .success:
                    print("log successfully. url : ",urlString, " ", params, ", res : ", res as Any)
                    success?(res)
                    
                case .failure(let err):
                    print("fail to log. url : ",urlString, " ", params)
                    print(err)
                    failure?(res,err)
                }
            })
    }
    
    // MARK: public
    func registerCheckInRecord(rloc: String,
                               airline: String,
                               flightNumber: String,
                               flightDate: String,
                               firstName: String,
                               lastName: String,
                               memberNumber: String,
                               memberGrade: String,
                               sequenceNumber: Int,
                               success: ((Int?) -> Void)? = nil,
                               failure: ((Int?, Error) -> Void)? = nil) {
        
        let urlString = "/registerCheckInRecord";
        let params = [
            "rloc": rloc,
            "airline": airline,
            "flightNumber": flightNumber,
            "flightDate": flightDate,
            "firstName": firstName,
            "lastName": lastName,
            "memberNumber": memberNumber,
            "memberGrade": memberGrade,
            "sequenceNumber": sequenceNumber
            ] as [String : Any]

        sendRequests(urlString: urlString,
                     params: params,
                     httpMethod: .post,
                     success: { (res) in
                        success?(res.response?.statusCode)
                        },
                     failure: { (res, err) in
                        failure?(res.response?.statusCode, err)
            })
    }
    
    func registerDevice(rloc: String,
                        airline: String,
                        flightNumber: String,
                        flightDate: String,
                        firstName: String,
                        lastName: String,
                        sequenceNumber: Int,
                        deviceId: String,
                        platform: Int,
                        osVersion: String,
                        model: String,
                        deviceToken: String,
                        fcmToken: String,
                        success: ((Int?) -> Void)? = nil,
                        failure: ((Int?, Error) -> Void)? = nil) {
        
        let urlString = "/registerDevice";
        let params = [
            "rloc": rloc,
            "airline": airline,
            "flightNumber": flightNumber,
            "flightDate": flightDate,
            "firstName": firstName,
            "lastName": lastName,
            "sequenceNumber": sequenceNumber,
            "deviceId": deviceId,
            "platform": platform,
            "osVersion": osVersion,
            "model": model,
            "deviceToken": deviceToken,
            "fcmToken": fcmToken
            ] as [String : Any]

        sendRequests(urlString: urlString,
                     params: params,
                     httpMethod: .post, success: { (res) in
                        success?(res.response?.statusCode)
                        },
                     failure: { (res, err) in
                        failure?(res.response?.statusCode, err)
        })
        
    }
    
    func unregisterDevice(rloc: String,
                          airline: String,
                          flightNumber: String,
                          flightDate: String,
                          firstName: String,
                          lastName: String,
                          sequenceNumber: Int,
                          deviceId: String,
                          platform: Int,
                          osVersion: String,
                          model: String,
                          success: ((Int?) -> Void)? = nil,
                          failure: ((Int?, Error) -> Void)? = nil) {
        
        let urlString = "/unregisterDevice";
        let params = [
            "rloc": rloc,
            "airline": airline,
            "flightNumber": flightNumber,
            "flightDate": flightDate,
            "firstName": firstName,
            "lastName": lastName,
            "sequenceNumber": sequenceNumber,
            "deviceId": deviceId,
            "platform": platform,
            "osVersion": osVersion,
            "model": model
            ] as [String : Any]

        sendRequests(urlString: urlString,
                     params: params,
                     httpMethod: .post, success: { (res) in
                        success?(res.response?.statusCode)
                        },
                     failure: { (res, err) in
                        failure?(res.response?.statusCode, err)
        })
    }
    
    func getFlightStatus(airline: String,
                         flightNumber: String,
                         flightDate: String,
                         success: ((Int?, FlightStatus?) -> Void)? = nil,
                         failure: ((Int?, Error) -> Void)? = nil)  {
        
        let urlString = "/getFlightStatus";
        let params = [
            "airline": airline,
            "flightNumber": flightNumber,
            "flightDate": flightDate
            ] as [String : Any]
        
        sendRequests(urlString: urlString,
                     params: params,
                     httpMethod: .get,
                     success: { (res) in
                        let json = res.result.value as! [String: Any?]
                        print(json)
                        let flightStatus = FlightStatus(airline: json["airline"]! as! String,
                                                        flightNumber: json["flightNumber"]! as! String,
                                                        flightDate: stringToDate(json["flightDate"]! as! String)!,
                                                        scheduledDepartureDatetime: stringToDate(json["scheduledDepartureDatetime"]! as! String),
                                                        estimatedDepartureDatetime: stringToDate(json["estimatedDepartureDatetime"]! as! String),
                                                        gateNumber: json["gateNumber"]! as? String ?? "",
                                                        departureAirport: json["departureAirport"]! as! String,
                                                        arrivalAirport: json["arrivalAirport"]! as! String,
                                                        boardingStatus: BoardingStatus(rawValue: json["boardingStatus"] as! Int16)!)
                        
                        success?(res.response?.statusCode,flightStatus)
                        },
                     failure: { (res, err) in
                        failure?(res.response?.statusCode, err)
        })
    }
    
    func getCheckInRecordList(success: ((Int?, [[String:Any?]]) -> Void)? = nil,
                              failure: ((Int?, Error) -> Void)? = nil) {
        
        let urlFormat = "/getCheckInRecordList?deviceId=%@&platform=%d&osVersion=%@&model=%@";
        let urlString = String(format: urlFormat, UIDevice.current.deviceId,1,UIDevice.current.systemVersion,UIDevice.current.hardwareModel())
        
        let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        sendRequests(urlString: encodedUrl!,
                     params: nil,
                     httpMethod: .get,
                     success: { (res) in
                        let json = res.result.value as! [[String:Any?]]
                        print(json)
                        success?(res.response?.statusCode, json)
        },
                     failure: { (res, err) in
                        failure?(res.response?.statusCode, err)
        })
    }
}
