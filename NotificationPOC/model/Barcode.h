//
//  Barcode.h
//  VMAppWithKonylib
//
//  Created by penny on 22/4/15.
//
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    PDF_417,
    QR
} barcodeType;

typedef enum : NSUInteger {
    BOARDING_PASS,
    INVITATION_CARD
} cardType;

@interface Barcode : NSObject{
}

@property (nonatomic,retain) NSString* bcodeData;

//@property barcodeType bcodeType;
//@property cardType cardType;

@property (readonly) NSString *rloc;
@property (readonly) NSString *customerName;
@property (readonly) NSString *origin;
@property (readonly) NSString *destination;
@property (readonly) NSString *firstName;
@property (readonly) NSString *lastName;
@property (readonly) NSString *flightNo;
@property (readonly) NSString *flightClass;
@property (readonly) NSString *dateOfFlightInJulianDate;
@property (readonly) NSString *opAirline;
@property (readonly) NSString *memberNo;
@property (readonly) NSString *memberGrade;
@property (readonly) NSString *memAirline;
@property (readonly) NSString *seqNo;
@property (readonly) barcodeType bcodeType;
@property (readonly) cardType inviteType;

-(id) initWithBarcodeData:(NSString *)data ofType:(barcodeType) type;
- (NSDate*) dateOfFlight;
- (NSString*) dateOfFlightString;
- (NSString*) toString;
- (NSString*) description;

@end
