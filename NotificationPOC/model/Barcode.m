#import "Barcode.h"

@implementation Barcode
@synthesize bcodeData = _bcodeData;
@synthesize rloc, origin, destination, customerName, firstName, lastName, flightNo, flightClass, dateOfFlightInJulianDate, opAirline, memberNo, memberGrade, memAirline, seqNo, inviteType, bcodeType;

-(id) initWithBarcodeData:(NSString *)data ofType:(barcodeType) type{
    if(self = [super init]){
        _bcodeData = data;
        bcodeType = type;
        
        //  IATA BCBP Standard M Model v4
        //  http://www.iata.org/whatwedo/stb/documents/bcbp_implementation_guidev4_jun2009.pdf (page 49)
        
        //  NOTE: regarding the conditional below, we've found that the QRCode length for some samples
        //  will be 118 characters.
        
        // field values' meaning:
        // http://www.iata.org/whatwedo/stb/Documents/resolution792-june2010.pdf
        
        @try{
            NSMutableString *formattedBarcodeString = [data mutableCopy];
            
            //some normalization for day of year: compatible for two formats: numeric or `ddMMM`, for example, 13AUG
            NSCharacterSet *nonNumeric = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
            NSString *dateOfFlightString = [formattedBarcodeString substringWithRange:NSMakeRange(44, 3)];    //  item 46
            if ([dateOfFlightString rangeOfCharacterFromSet:nonNumeric].location != NSNotFound) {
                //
                dateOfFlightString = [formattedBarcodeString substringWithRange:NSMakeRange(44, 5)];
                NSDateFormatter *df = [[NSDateFormatter alloc]init];
                df.dateFormat = @"ddMMM";
                NSDate *date = [df dateFromString:dateOfFlightString];
                
                NSCalendar *calenar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSInteger dayOfYear = [calenar ordinalityOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitYear forDate:date];
                
                NSLog(@"dayOfYear : %ld",dayOfYear);
                [formattedBarcodeString replaceCharactersInRange:NSMakeRange(44, 5) withString:[@(dayOfYear) stringValue]];
                NSLog(@"formattedBarcodeString : %@",formattedBarcodeString);
            }
            
            
            inviteType = INVITATION_CARD;
            if(formattedBarcodeString.length > 71 + 1 && [@"B" isEqualToString:[formattedBarcodeString substringWithRange:NSMakeRange(71, 1)]]){ // item 16
                inviteType = BOARDING_PASS;
            }
            
            //things before item 16
            customerName = [formattedBarcodeString substringWithRange:NSMakeRange(2, 20)];    //  item 11
            rloc = [formattedBarcodeString substringWithRange:NSMakeRange(23, 7)];
            origin = [formattedBarcodeString substringWithRange:NSMakeRange(30, 3)];          //  item 26
            destination = [formattedBarcodeString substringWithRange:NSMakeRange(33, 3)];     //  item 38
            opAirline = [formattedBarcodeString substringWithRange:NSMakeRange(36, 2)];       //  item 42
            flightNo = [formattedBarcodeString substringWithRange:NSMakeRange(39, 4)];        //  item 43
            dateOfFlightInJulianDate = [formattedBarcodeString substringWithRange:NSMakeRange(44, 3)];    //  item 46
            flightClass = [formattedBarcodeString substringWithRange:NSMakeRange(47, 1)];     //  item 71
            seqNo = [formattedBarcodeString substringWithRange:NSMakeRange(52, 5)];           //  item 107
            
            unsigned firstSegmentLength = 0;
            if(formattedBarcodeString.length > 62 + 2){
                NSScanner *scanner = [NSScanner scannerWithString:[formattedBarcodeString substringWithRange:NSMakeRange(62, 2)]];  //  item 10
                [scanner scanHexInt:&firstSegmentLength];
                
                //  62, index of item 10,
                // + 2, length of item 10,
                // + firstSegmentLength,
                // + 2. length of item 17
                int secondSegmentOffset = 64 + firstSegmentLength + 2;
                
                unsigned secondSegmentLength = 0;
                NSString *secondSegmentLengthHex = @"  ";
                if(formattedBarcodeString.length > 64+firstSegmentLength+2){
                    secondSegmentLengthHex = [formattedBarcodeString substringWithRange:NSMakeRange(64 + firstSegmentLength, 2)]; // item 17
                }
                if([@"  " isEqualToString:secondSegmentLengthHex]){
                    secondSegmentOffset = 77;
                    secondSegmentLength = 41;
                } else{
                    scanner = [NSScanner scannerWithString:secondSegmentLengthHex];
                    [scanner scanHexInt:&secondSegmentLength];
                }
                
                @try {
                    memAirline = [[formattedBarcodeString substringWithRange:NSMakeRange(secondSegmentOffset + 18, 3)] stringByReplacingOccurrencesOfString:@" " withString:@""];
                }
                @catch (NSException *e){
                    memAirline = @"";
                }
                
                @try {
                    memberNo = [formattedBarcodeString substringWithRange:NSMakeRange(secondSegmentOffset + 21, 16)];
                }
                @catch (NSException *e){
                    memberNo = @"";
                }
                
                NSString *membershipInfo = @"";
                
                @try {
                    membershipInfo = [formattedBarcodeString substringWithRange:NSMakeRange(secondSegmentOffset + secondSegmentLength, 3)]; //item 4
                }
                @catch (NSException *e){
                    membershipInfo = @"";
                }
                
                if(![@"" isEqualToString:memAirline] && ![@"" isEqualToString:membershipInfo]){
                    if([@"CX" isEqualToString:memAirline]) {
                        memberGrade = [membershipInfo substringFromIndex:1];
                    } else {
                        NSString *gradeNo = [membershipInfo substringToIndex:1];
                        if([gradeNo intValue] > 0){
                            switch ([gradeNo intValue]) {
                                case 1:
                                    memberGrade = @"EM";
                                    break;
                                case 2:
                                    memberGrade = @"SA";
                                    break;
                                case 3:
                                    memberGrade = @"RU";
                                    break;
                                default:
                                    memberGrade = @"";
                                    break;
                            }
                        } else {
                            memberGrade = @"";
                        }
                    }
                }
            }
        }@catch(NSException *){
            NSLog(@"in exception while scanning boarding/invite pass");
        }
        
        origin = [origin stringByReplacingOccurrencesOfString:@" " withString:@""];
        destination = [destination stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        firstName = [firstName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        lastName = [lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        memberNo = [memberNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        firstName = customerName;
        if([customerName containsString:@"/"]){
            NSArray *sName = [customerName componentsSeparatedByString:@"/"];
            lastName = sName[0];
            firstName = sName[1];
        }
        
        // remove leading zero of seqno
        NSRange range = [seqNo rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        seqNo = [seqNo stringByReplacingCharactersInRange: range withString:@""];
        
        //  clean up spacing
        memberNo = [memberNo stringByReplacingOccurrencesOfString:@" " withString:@""];
        memberGrade = [memberGrade stringByReplacingOccurrencesOfString:@" " withString:@""];
        opAirline = [opAirline stringByReplacingOccurrencesOfString:@" " withString:@""];
        seqNo = [seqNo stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        //  strip the leading 0 of flight numbers similar to CX0123
        if([[flightNo substringWithRange:NSMakeRange(3, 1)] isEqualToString:@"0"]) {
            flightNo = [[flightNo substringToIndex:3] stringByAppendingString:[flightNo substringFromIndex:4]];
            flightNo = [NSString stringWithFormat:@"%04ld", (long)flightNo.integerValue];
            NSLog(@"flightNo : %@", flightNo);
        }
    }
    return self;
}

- (void)handleItem{
    inviteType = INVITATION_CARD;
    if(_bcodeData.length > 71 + 1 && [@"B" isEqualToString:[_bcodeData substringWithRange:NSMakeRange(71, 1)]]){ // item 16
        inviteType = BOARDING_PASS;
    }
    //things before item 16
    customerName = [_bcodeData substringWithRange:NSMakeRange(2, 20)];    //  item 11
    origin = [_bcodeData substringWithRange:NSMakeRange(30, 3)];          //  item 26
    destination = [_bcodeData substringWithRange:NSMakeRange(33, 3)];     //  item 38
    opAirline = [_bcodeData substringWithRange:NSMakeRange(36, 2)];       //  item 42
    flightNo = [_bcodeData substringWithRange:NSMakeRange(36, 7)];        //  item 43
    dateOfFlightInJulianDate = [_bcodeData substringWithRange:NSMakeRange(44, 3)];    //  item 46
    flightClass = [_bcodeData substringWithRange:NSMakeRange(47, 1)];     //  item 71
    seqNo = [_bcodeData substringWithRange:NSMakeRange(52, 5)];           //  item 107
}

- (void)handleInfo{
    unsigned firstSegmentLength = 0;
    if(_bcodeData.length > 62 + 2){
        NSScanner *scanner = [NSScanner scannerWithString:[_bcodeData substringWithRange:NSMakeRange(62, 2)]];  //  item 10
        [scanner scanHexInt:&firstSegmentLength];
        int secondSegmentOffset = 64 + firstSegmentLength + 2;
        unsigned secondSegmentLength = 0;
        NSString *secondSegmentLengthHex = @"  ";
        if(_bcodeData.length > 64+firstSegmentLength+2){
            secondSegmentLengthHex = [_bcodeData substringWithRange:NSMakeRange(64 + firstSegmentLength, 2)]; // item 17
        }
        if([@"  " isEqualToString:secondSegmentLengthHex]){
            secondSegmentOffset = 77;
            secondSegmentLength = 41;
        } else{
            scanner = [NSScanner scannerWithString:secondSegmentLengthHex];
            [scanner scanHexInt:&secondSegmentLength];
        }
        @try {
            memAirline = [[_bcodeData substringWithRange:NSMakeRange(secondSegmentOffset + 18, 3)] stringByReplacingOccurrencesOfString:@" " withString:@""];
        }
        @catch (NSException *e){
            memAirline = @"";
        }
        @try {
            memberNo = [_bcodeData substringWithRange:NSMakeRange(secondSegmentOffset + 21, 16)];
        }
        @catch (NSException *e){
            memberNo = @"";
        }
        NSString *membershipInfo = @"";
        @try {
            membershipInfo = [_bcodeData substringWithRange:NSMakeRange(secondSegmentOffset + secondSegmentLength, 3)]; //item 4
        }
        @catch (NSException *e){
            membershipInfo = @"";
        }
        [self handleBcode:membershipInfo];
    }
}

- (void)handleBcode:(NSString *)membershipInfo{
    if(![@"" isEqualToString:memAirline] && ![@"" isEqualToString:membershipInfo]){
        if([@"CX" isEqualToString:memAirline]) {
            memberGrade = [membershipInfo substringFromIndex:1];
        } else {
            NSString *gradeNo = [membershipInfo substringToIndex:1];
            if([gradeNo intValue] > 0){
                switch ([gradeNo intValue]) {
                    case 1:
                        memberGrade = @"EM";
                        break;
                    case 2:
                        memberGrade = @"SA";
                        break;
                    case 3:
                        memberGrade = @"RU";
                        break;
                    default:
                        memberGrade = @"";
                        break;
                }
            } else {
                memberGrade = @"";
            }
        }
    }
}

- (void)handleData{
    rloc = [origin stringByReplacingOccurrencesOfString:@" " withString:@""];
    origin = [origin stringByReplacingOccurrencesOfString:@" " withString:@""];
    destination = [destination stringByReplacingOccurrencesOfString:@" " withString:@""];
    firstName = [firstName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    lastName = [lastName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    memberNo = [memberNo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    firstName = customerName;
    if([customerName containsString:@"/"]){
        NSArray *sName = [customerName componentsSeparatedByString:@"/"];
        lastName = sName[0];
        firstName = sName[1];
    }
    // remove leading zero of seqno
    NSRange range = [seqNo rangeOfString:@"^0*" options:NSRegularExpressionSearch];
    seqNo = [seqNo stringByReplacingCharactersInRange: range withString:@""];
    //  clean up spacing
    memberNo = [memberNo stringByReplacingOccurrencesOfString:@" " withString:@""];
    memberGrade = [memberGrade stringByReplacingOccurrencesOfString:@" " withString:@""];
    opAirline = [opAirline stringByReplacingOccurrencesOfString:@" " withString:@""];
    seqNo = [seqNo stringByReplacingOccurrencesOfString:@" " withString:@""];
    //  strip the leading 0 of flight numbers similar to CX0123
    if([[flightNo substringWithRange:NSMakeRange(3, 1)] isEqualToString:@"0"]) {
        flightNo = [[flightNo substringToIndex:3] stringByAppendingString:[flightNo substringFromIndex:4]];
        flightNo = [NSString stringWithFormat:@"%04ld", (long)flightNo.integerValue];
        NSLog(@"flightNo : %@", flightNo);
    }
}

- (NSDate*)dateOfFlight {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyDDD"];
    NSString *dateStr = [NSString stringWithFormat:@"%ld",(18000+self.dateOfFlightInJulianDate.integerValue)];
    NSDate *aDate = [fmt dateFromString:dateStr];
    NSLog(@"date : %@", aDate);
    return aDate;
}

- (NSString*)dateOfFlightString {
    return [self datetimeToString:self.dateOfFlight];
}

- (NSString*)datetimeToString:(NSDate*)date
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-MM-dd"];
    NSString *str = [fmt stringFromDate:date];
    return str;
}

- (NSString*) toString{
    NSError *error;
    NSArray *keys = @[@"bcodeType", @"inviteType", @"rloc", @"firstName", @"lastName", @"origin", @"destination", @"flightNo", @"flightClass", @"airline", @"memberNo", @"memberGrade", @"seqNo", @"dateOfFlight"];
    NSArray *objects = @[(self.bcodeType == QR)?@"QR":@"PDF_417", (self.inviteType == BOARDING_PASS)?@"BOARDING_PASS":@"INVITATION_CARD", self.rloc, self.firstName, self.lastName, self.origin, self.destination, self.flightNo, self.flightClass, self.opAirline,self.memberNo, self.memberGrade == nil ? @"" : self.memberGrade, self.seqNo, self.dateOfFlightString];
    
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjects:objects
                                                               forKeys:keys];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[[NSMutableArray alloc] initWithObjects:jsonDictionary, nil] options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

- (NSString*) description{
    return [self toString];
}
@end
