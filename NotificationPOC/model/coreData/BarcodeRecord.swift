//
//  BarCodeRecord.swift
//  NotificationPOC
//
//  Created by Accenture on 27/12/2017.
//  Copyright © 2017 Accenture. All rights reserved.
//

import Foundation
import CoreData

extension BarcodeRecord {
    static private var managedContext : NSManagedObjectContext {
        get {
            return CoreDataStack.shared.persistentContainer.viewContext
        }
    }
    static private var entityDescription : NSEntityDescription {
        get {
            return NSEntityDescription.entity(forEntityName: String(describing: self), in: managedContext)!
        }
    }
    
    static func selectAll() -> [BarcodeRecord]? {
        
        let request = NSFetchRequest<BarcodeRecord>(entityName: String(describing: self))
        
        do {
            let result = try managedContext.fetch(request)
            return result
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    static func selectRecord(rloc: String,
                             firstName: String,
                             lastName: String,
                             airline: String,
                             flightNumber : String,
                             flightDate : String,
                             sequenceNumber : Int16) -> BarcodeRecord? {
        
        let request = NSFetchRequest<BarcodeRecord>(entityName: String(describing: self))
        
        request.predicate = NSPredicate(format: "(rloc == %@) && (firstName == %@) && (lastName == %@) && (airline == %@) && (flightNumber == %@) && (flightDate == %@) && (sequenceNumber == %d)", rloc, firstName, lastName, airline, flightNumber, flightDate, sequenceNumber)
        
        do {
            let result = try managedContext.fetch(request)
            
            print(result.count)
            guard result.count == 1 else {
                return nil
            }
            
            return result[0]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func delete()  {
        
        BarcodeRecord.managedContext.delete(self)
        CoreDataStack.shared.saveContext()
    }
    
    static func add(rloc : String,
                    airline : String,
                    flightNumber : String,
                    flightDate : String,
                    firstName : String,
                    lastName : String,
                    sequenceNumber : Int16,
                    deviceId : String,
                    platform : Int16,
                    osVersion : String,
                    model : String,
                    memberNumber: String,
                    memberGrade: String,
                    scheduledDepartureDatetime : String?,
                    estimatedDepartureDatetime : String?,
                    gateNumber : String?,
                    departureAirport : String,
                    arrivalAirport : String,
                    boardingStatus : Int16) -> Void{
        
        let barcodeRecord = BarcodeRecord(entity: self.entityDescription, insertInto: self.managedContext)
        
        barcodeRecord.rloc = rloc
        barcodeRecord.airline = airline
        barcodeRecord.flightNumber = String(format: "%04d", Int(flightNumber)!)
        barcodeRecord.flightDate = flightDate
        barcodeRecord.firstName = firstName
        barcodeRecord.lastName = lastName
        barcodeRecord.sequenceNumber = sequenceNumber
        barcodeRecord.deviceId = deviceId
        barcodeRecord.platform = platform
        barcodeRecord.osVersion = osVersion
        barcodeRecord.model = model
        barcodeRecord.memberNumber = memberNumber
        barcodeRecord.memberGrade = memberGrade
        barcodeRecord.scheduledDepartureDatetime = scheduledDepartureDatetime
        barcodeRecord.estimatedDepartureDatetime = estimatedDepartureDatetime
        barcodeRecord.gateNumber = gateNumber
        barcodeRecord.departureAirport = departureAirport
        barcodeRecord.arrivalAirport = arrivalAirport
        barcodeRecord.boardingStatus = boardingStatus
        
        CoreDataStack.shared.saveContext()
    }
    
    static func updateOrAdd(rloc : String,
                            airline : String,
                            flightNumber : String,
                            flightDate : String,
                            firstName : String,
                            lastName : String,
                            sequenceNumber : Int16,
                            deviceId : String,
                            platform : Int16,
                            osVersion : String,
                            model : String,
                            memberNumber: String,
                            memberGrade: String,
                            scheduledDepartureDatetime : String?,
                            estimatedDepartureDatetime : String?,
                            gateNumber : String?,
                            departureAirport : String,
                            arrivalAirport : String,
                            boardingStatus : Int16) {
        
        if let barcodeRecord = selectRecord(rloc: rloc, firstName: firstName, lastName: lastName, airline: airline, flightNumber: flightNumber, flightDate: flightDate, sequenceNumber: sequenceNumber) {
            //exist in db, update fields
            barcodeRecord.update(rloc: rloc, airline: airline, flightNumber: flightNumber, flightDate: flightDate, firstName: firstName, lastName: lastName, sequenceNumber: sequenceNumber, deviceId: deviceId, platform: platform, osVersion: osVersion, model: model, memberNumber: memberNumber, memberGrade: memberGrade, scheduledDepartureDatetime: scheduledDepartureDatetime, estimatedDepartureDatetime: estimatedDepartureDatetime, gateNumber: gateNumber, departureAirport: departureAirport, arrivalAirport: arrivalAirport, boardingStatus: boardingStatus)
            
            
            
        } else {
            //not exist, insert to db
            add(rloc: rloc, airline: airline, flightNumber: flightNumber, flightDate: flightDate, firstName: firstName, lastName: lastName, sequenceNumber: sequenceNumber, deviceId: deviceId, platform: platform, osVersion: osVersion, model: model, memberNumber: memberNumber, memberGrade: memberGrade, scheduledDepartureDatetime: scheduledDepartureDatetime, estimatedDepartureDatetime: estimatedDepartureDatetime, gateNumber: gateNumber, departureAirport: departureAirport, arrivalAirport: arrivalAirport, boardingStatus: boardingStatus)
        }
        
        
    }
    
    func update(rloc : String,
                airline : String,
                flightNumber : String,
                flightDate : String,
                firstName : String,
                lastName : String,
                sequenceNumber : Int16,
                deviceId : String,
                platform : Int16,
                osVersion : String,
                model : String,
                memberNumber: String,
                memberGrade: String,
                scheduledDepartureDatetime : String?,
                estimatedDepartureDatetime : String?,
                gateNumber : String?,
                departureAirport : String,
                arrivalAirport : String,
                boardingStatus : Int16) {
        
        let updatedFieldStatus = BarcodeRecord.getFieldStatus(oldRecord: self, newGateNumber: gateNumber, newEstimatedDepartureDatetime: estimatedDepartureDatetime, newScheduledDepartureDatetime: scheduledDepartureDatetime, newBoardingStatus: boardingStatus)
        
        self.rloc = rloc
        self.airline = airline
        self.flightNumber = flightNumber
        self.flightDate = flightDate
        self.firstName = firstName
        self.lastName = lastName
        self.sequenceNumber = sequenceNumber
        self.deviceId = deviceId
        self.platform = platform
        self.osVersion = osVersion
        self.model = model
        self.memberNumber = memberNumber
        self.memberGrade = memberGrade
        self.scheduledDepartureDatetime = scheduledDepartureDatetime
        self.estimatedDepartureDatetime = estimatedDepartureDatetime
        self.gateNumber = gateNumber
        self.departureAirport = departureAirport
        self.arrivalAirport = arrivalAirport
        self.boardingStatus = boardingStatus
        self.fieldUpdateStatus = updatedFieldStatus
        
        CoreDataStack.shared.saveContext()
    }
    
    func resetStatusField() {
        self.fieldUpdateStatus = 0
        CoreDataStack.shared.saveContext()
    }
    
    func isChanged() -> Bool {
        return isGateChanged() || isEstimatedDepartureTimeChanged() || isScheduledDepartureTimeChanged() || isBoardingStatusChanged()
    }
    
    func isGateChanged() -> Bool{
        return isFieldStatusBitOn(0)
    }
    
    func isEstimatedDepartureTimeChanged() -> Bool{
        return isFieldStatusBitOn(1)
    }
    
    func isScheduledDepartureTimeChanged() -> Bool{
        return isFieldStatusBitOn(2)
    }
    
    func isBoardingStatusChanged() -> Bool{
        return isFieldStatusBitOn(3)
    }
    
    static func getFieldStatus(oldRecord: BarcodeRecord, newGateNumber: String?, newEstimatedDepartureDatetime: String?, newScheduledDepartureDatetime: String?, newBoardingStatus: Int16) -> Int32 {
        /*Bit 0 : Gate Number,
         Bit 1 : Estimated Departure Time,
         Bit 2 : Scheduled Departure Time,
         Bit 3 : Bording Status*/
        
        var status = Int32(0)
        let isGateNumberBitOn = (oldRecord.gateNumber != newGateNumber || oldRecord.isGateChanged())
        let isEstimatedTimeBitOn = (oldRecord.estimatedDepartureDatetime != newEstimatedDepartureDatetime || oldRecord.isEstimatedDepartureTimeChanged())
        let isScheduledTimeBitOn = (oldRecord.scheduledDepartureDatetime != newScheduledDepartureDatetime || oldRecord.isScheduledDepartureTimeChanged())
        let isBoardingStatusBitOn = (oldRecord.boardingStatus != newBoardingStatus || oldRecord.isBoardingStatusChanged())
        status += (isGateNumberBitOn ? 1 : 0)
        status += (isEstimatedTimeBitOn ? 1 : 0) << 1
        status += (isScheduledTimeBitOn ? 1 : 0) << 2
        status += (isBoardingStatusBitOn ? 1 : 0) << 3
        
        return status
    }
    
    func isFieldStatusBitOn(_ position:Int) -> Bool{
        
        let statusInt = self.fieldUpdateStatus
        let filter = Int32(1 << position)
        let result = (statusInt & filter) >> position
        return result == 1;
    }
    
    
}
