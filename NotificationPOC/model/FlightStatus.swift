//
//  FlightStatus.swift
//  NotificationPOC
//
//  Created by Accenture on 4/1/2018.
//  Copyright © 2018 Accenture. All rights reserved.
//

import Foundation

enum BoardingStatus : Int16 {
    case NoStatus = 0
    case BoardingSoon = 1
    case BoardingNow = 2
    case Closed = 3
    
    var description : String {
        switch self {
        case .NoStatus:
            return "No Status"
        case .BoardingSoon:
            return "Boarding Soon"
        case .BoardingNow:
            return "Boarding Now"
        case .Closed:
            return "Closed"
        }
    }
}

class FlightStatus {
    
    var airline : String
    var flightNumber : String
    var flightDate : Date
    var scheduledDepartureDatetime : Date?
    var estimatedDepartureDatetime : Date?
    var gateNumber : String?
    var departureAirport : String
    var arrivalAirport : String
    var boardingStatus : BoardingStatus
    
    init(airline : String, flightNumber : String, flightDate : Date, scheduledDepartureDatetime : Date? = nil, estimatedDepartureDatetime : Date? = nil, gateNumber : String? = nil, departureAirport : String, arrivalAirport : String, boardingStatus : BoardingStatus)
    {
        self.airline = airline
        self.flightNumber = flightNumber
        self.flightDate = flightDate
        self.scheduledDepartureDatetime = scheduledDepartureDatetime
        self.estimatedDepartureDatetime = estimatedDepartureDatetime
        self.gateNumber = gateNumber
        self.departureAirport = departureAirport
        self.arrivalAirport = arrivalAirport
        self.boardingStatus = boardingStatus
    }
}
